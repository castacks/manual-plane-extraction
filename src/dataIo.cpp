#include "plane_seg/dataIo.h"

#include <iostream>
#include <fstream>

#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>


using namespace  std;


bool dataIo::readPointCloud(const std::string &fileName, pointCloudXYZ &pointCloud)
{
	ifstream ifs(fileName.data());
	int ptNum;
	ifs >> ptNum;
cout<<ptNum<<endl;
	if (ptNum != 4)
	{
		cout << "the point number must be 4!" << endl;
		return false;
	}
	for (int i = 0; i < ptNum; ++i)
	{
		pcl::PointXYZ pt;
		ifs >> pt.x >> pt.y >> pt.z;
		pointCloud.points.push_back(pt);
	}

	return true;
}

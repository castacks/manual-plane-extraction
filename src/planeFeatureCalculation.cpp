#include "plane_seg/planeFeatureCalculation.h"

#include <set>
#include <list>
#include <deque>
#include <fstream>
#include <vector>
#include <pcl/point_types.h>

#include <opencv2/opencv.hpp>
#include <pcl/visualization/pcl_visualizer.h>

using namespace  std;

void planeFeaCal::calculateTheFeaturesOfPlanes(const pointCloudXYZ &pointCloud,planeFeature &planeFea)
{
	calculateEigenFeasByPCA(pointCloud, planeFea);
	planeBoxBound3d  planeBound;
	calculateTheBoundOfPlane(pointCloud, planeFea.bound);
}


void planeFeaCal::calculateEigenFeasByPCA(const pointCloudXYZ &planePointCloud, planeFeature &planeFea)
{
	CvMat* pData = cvCreateMat(planePointCloud.points.size(), 3, CV_32FC1);
	CvMat* pMean = cvCreateMat(1, 3, CV_32FC1);
	CvMat* pEigVals = cvCreateMat(1, 3, CV_32FC1);
	CvMat* pEigVecs = cvCreateMat(3, 3, CV_32FC1);

	//covariance matrix;
	for (size_t j = 0; j < planePointCloud.points.size(); ++j)
	{
		cvmSet(pData, j, 0, planePointCloud.points[j].x);
		cvmSet(pData, j, 1, planePointCloud.points[j].y);
		cvmSet(pData, j, 2, planePointCloud.points[j].z);
	}

	//principle component analysis to get Eigen vectora and Eigen values;
	cvCalcPCA(pData, pMean, pEigVals, pEigVecs, CV_PCA_DATA_AS_ROW);

	//eliminate the ambiguity of normal vector;
	//the include angle between the normal and the direction of center point to origion must be acute angle;
	double plCeterx, plCetery, plCeterz;//center point of the plane;
	Eigen::Vector3f normal, plCeter2Origion;//the direction from center point to origion;
	plCeterx = cvmGet(pMean, 0, 0);
	plCetery = cvmGet(pMean, 0, 1);
	plCeterz = cvmGet(pMean, 0, 2);
	plCeter2Origion.x() = -plCeterx;
	plCeter2Origion.y() = -plCetery;
	plCeter2Origion.z() = -plCeterz;

	//the normal 
	normal.x() = cvmGet(pEigVecs, 2, 0);
	normal.y() = cvmGet(pEigVecs, 2, 1);
	normal.z() = cvmGet(pEigVecs, 2, 2);
	float dotProduct;
	dotProduct = normal.dot(plCeter2Origion);
	//if the include angle is obtuse angle, the direction of normal will be flipped;
	float dist;
	dist = sqrt(normal.x()*normal.x() + normal.y()*normal.y() + normal.z()*normal.z());
	if (dotProduct < 0.0f)
	{
		planeFea.planeFun(0) = -normal.x() / dist;
		planeFea.planeFun(1) = -normal.y() / dist;
		planeFea.planeFun(2) = -normal.z() / dist;
	}
	else
	{
		planeFea.planeFun(0) = normal.x() / dist;
		planeFea.planeFun(1) = normal.y() / dist;
		planeFea.planeFun(2) = normal.z() / dist;
	}
	//distance from original to plane;
	double a, b, c;
	a = planeFea.planeFun(0);
	b = planeFea.planeFun(1);
	c = planeFea.planeFun(2);
	planeFea.planeFun(3) = -(a*plCeterx + b*plCetery + c*plCeterz);

	planeFea.minEigenValue = cvmGet(pEigVals, 0, 2);//the minimum eigen value;
	planeFea.centerPt.x() = plCeterx;
	planeFea.centerPt.y() = plCetery;
	planeFea.centerPt.z() = plCeterz;
	planeFea.ptNum = planePointCloud.points.size();

	cvReleaseMat(&pEigVecs);
	cvReleaseMat(&pEigVals);
	cvReleaseMat(&pMean);
	cvReleaseMat(&pData);
}

void planeFeaCal::calculateTheBoundOfPlane(const pointCloudXYZ &pointCloud, planeBoxBound3d &planeBound)
{
	for (size_t i = 0; i < pointCloud.points.size(); ++i)
	{
		planeBound.pt[i] = pointCloud.points[i];
	}
}

void planeFeaCal::displayPlaneAndBound(const pointCloudXYZ &pointCloud, const planeFeature &planeFea)
{
	srand((unsigned)time(NULL));
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudRGB(new pcl::PointCloud<pcl::PointXYZRGB>);

	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("planes"));
	viewer->setBackgroundColor(255, 255, 255);

	int R, G, B;
	R = rand() % 255;
	G = rand() % 255;
	B = rand() % 255;

	for (size_t i = 0; i < pointCloud.size(); ++i)
	{
		pcl::PointXYZRGB TempPoint;
		TempPoint.x = pointCloud.points[i].x;
		TempPoint.y = pointCloud.points[i].y;
		TempPoint.z = pointCloud.points[i].z;
		TempPoint.r = R;
		TempPoint.g = G;
		TempPoint.b = B;
		cloudRGB->points.push_back(TempPoint);
	}
	viewer->addPointCloud(cloudRGB);


	int n = 0;
	char t[256];
	string s;


	pcl::PointXYZ pt1, pt2;
	for (int j = 0; j < 3; j++)
	{
		pt1.x = planeFea.bound.pt[j].x;
		pt1.y = planeFea.bound.pt[j].y;
		pt1.z = planeFea.bound.pt[j].z;

		pt2.x = planeFea.bound.pt[j + 1].x;
		pt2.y = planeFea.bound.pt[j + 1].y;
		pt2.z = planeFea.bound.pt[j + 1].z;

		sprintf(t, "%d", n);
		n++;
		s = t;
		viewer->addLine(pt1, pt2, 1.0, 0.0, 0.0, s);
		viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 2, s);

		sprintf(t, "%d", n);
		n++;
		s = t;
		viewer->addSphere(pt1, 0.5, 1.0, 0.0, 0.0, s);

	}

	pt1.x = planeFea.bound.pt[3].x;
	pt1.y = planeFea.bound.pt[3].y;
	pt1.z = planeFea.bound.pt[3].z;

	pt2.x = planeFea.bound.pt[0].x;
	pt2.y = planeFea.bound.pt[0].y;
	pt2.z = planeFea.bound.pt[0].z;

	sprintf(t, "%d", n);
	n++;
	s = t;
	viewer->addLine(pt1, pt2, 1.0, 0.0, 0.0, s);
	viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 2, s);

	sprintf(t, "%d", n);
	n++;
	s = t;
	viewer->addSphere(pt1, 0.5, 1.0, 0.0, 0.0, s);

	while (!viewer->wasStopped())
	{
		viewer->spinOnce();
		boost::this_thread::sleep(boost::posix_time::microseconds(1000));
	}
}


void planeFeaCal::outputPlanesFea(const string &filename, const planeFeaCal::planeFeature &planeFea)
{
    ofstream ofs;
	ofs.open(filename.data(),ios_base::out);
	ofs << setiosflags(ios::fixed) << setprecision(3) << planeFea.planeFun(0) << "  "
		<< setiosflags(ios::fixed) << setprecision(3) << planeFea.planeFun(1) << "  "
		<< setiosflags(ios::fixed) << setprecision(3) << planeFea.planeFun(2) << "  "
		<< setiosflags(ios::fixed) << setprecision(3) << planeFea.planeFun(3) << "  ";

	for (int j = 0; j < 4; ++j)
	{
		ofs << setiosflags(ios::fixed) << setprecision(3) << planeFea.bound.pt[j].x << "  "
			<< setiosflags(ios::fixed) << setprecision(3) << planeFea.bound.pt[j].y << "  "
			<< setiosflags(ios::fixed) << setprecision(3) << planeFea.bound.pt[j].z << "  ";
	}
	ofs << endl;

	ofs.close();
}
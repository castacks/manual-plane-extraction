#include "plane_seg/dataIo.h"
#include "plane_seg/planeFeatureCalculation.h"
#include <iostream>
#include <iomanip>
#include <ros/ros.h>
using namespace  std;

int main(int argc, char**argv)
{
	ros::init(argc, argv,"planeseg_node");
	ros::NodeHandle("~");
	//Input Point Cloud;
	dataIo io;
	pointCloudXYZ pointCloud;
	string input_pcd;
	ros::param::get("~input_pcd",input_pcd);
	cout<<input_pcd<<endl;
	io.readPointCloud(input_pcd, pointCloud);
	cout<< "readPointCloudFromPcdFileA........."<<endl;
	
	//plane feature calculation;
	planeFeaCal::planeFeature planeFea;
	planeFeaCal pfc;
	string plane_fea_txt;
	ros::param::get("~plane_fea_txt",plane_fea_txt);
	pfc.calculateTheFeaturesOfPlanes(pointCloud, planeFea);
	pfc.outputPlanesFea(plane_fea_txt,planeFea);
	pfc.displayPlaneAndBound(pointCloud, planeFea);
	cout<< "calculateTheFeaturesOfPlanes.........."<<endl;
	
	pointCloudXYZ().swap(pointCloud);
	cout<< "DONE..."<<endl;
	
    return 0;
}
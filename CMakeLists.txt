cmake_minimum_required(VERSION 2.8.3)
project(planesegmentationmanually)

find_package(catkin REQUIRED COMPONENTS
  pcl_conversions
  pcl_ros
  roscpp
  eigen_conversions
)

find_package(PCL REQUIRED)
find_package(Boost REQUIRED)
find_package(OpenCV 2.1 REQUIRED)

catkin_package(
  INCLUDE_DIRS include
  LIBRARIES
  DEPENDS system_lib
)


include_directories(
   include
  ${catkin_INCLUDE_DIRS}
  ${PCL_INCLUDE_DIRS}
  ${Boost_INCLUDE_DIRS}
  ${OpenCV_INCLUDE_DIRS}
)


add_library(dataIo_manual src/dataIo.cpp)
target_link_libraries(dataIo_manual ${catkin_LIBRARIES})

add_library(planeFeaCal_manual src/planeFeatureCalculation.cpp)
target_link_libraries(planeFeaCal_manual ${OpenCV_LIBRARIES} ${catkin_LIBRARIES})

add_executable(planeseg_node_manual src/plane_seg_manual_node.cpp)
target_link_libraries(planeseg_node_manual dataIo_manual planeFeaCal_manual ${PCL_LIBRARIES} ${OpenCV_LIBRARIES} ${catkin_LIBRARIES})

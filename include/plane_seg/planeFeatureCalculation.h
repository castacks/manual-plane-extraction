#ifndef PLANEFEATURECALCULATION_H
#define PLANEFEATURECALCULATION_H

#include "dataIo.h"

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/PointIndices.h>

#include <Eigen/Dense>

class planeFeaCal
{
public:
	enum PlaneRelation
	{
		DISJOINT=1,
		INTERSECTION,
		INCLUSIVE
	};

	struct planeBoxBound3d
	{
		pcl::PointXYZ pt[4];
		planeBoxBound3d()
		{
			for (int i = 0; i < 4; i++)
			{
				pt[i].x = pt[i].y = pt[i].z = 0.0f;
			}
		}
	};

	struct planeBoxBound2d
	{
		pcl::PointXY pt[4];
		planeBoxBound2d()
		{
			for (int i = 0; i < 4; i++)
			{
				pt[i].x = pt[i].y = 0.0f;
			}
		}
	};

	struct PlaneSclae
	{
		float area;
		float width;
		float length;
		PlaneSclae()
		{
			area = width = length = 0.0f;
		}
	};

	struct planeFeature
	{
		Eigen::Vector4f planeFun;
		Eigen::Vector3f centerPt;
		planeBoxBound3d bound;
		float minEigenValue;
		PlaneSclae planeScale;
		size_t ptNum;
		planeFeature()
		{
			minEigenValue = 0.0f;
			ptNum = 0;
		}
	};

	/*plane features calculation*/
	void calculateTheFeaturesOfPlanes(const pointCloudXYZ &pointCloud, planeFeature &planesFea);

	void calculateEigenFeasByPCA(const pointCloudXYZ &pointCloud, planeFeature &planeFea);

	void calculateTheBoundOfPlane(const pointCloudXYZ &pointCloud, planeBoxBound3d &bound);

	void outputPlanesFea(const std::string &filename, const planeFeaCal::planeFeature &planeFea);

	void displayPlaneAndBound(const pointCloudXYZ &pointCloud, const planeFeature  &planeFea);
};
#endif
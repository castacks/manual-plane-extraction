#ifndef DATAIO
#define DATAIO

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <string>

typedef  pcl::PointCloud<pcl::PointXYZ>::Ptr      pointCloudXYZPtr;
typedef  pcl::PointCloud<pcl::PointXYZ>           pointCloudXYZ;


typedef  pcl::PointCloud<pcl::PointXY>::Ptr      pointCloudXYPtr;
typedef  pcl::PointCloud<pcl::PointXY>           pointCloudXY;

typedef  pcl::PointCloud<pcl::PointXYZRGB>::Ptr      pointCloudXYZRGBPtr;
typedef  pcl::PointCloud<pcl::PointXYZRGB>           pointCloudXYZRGB;


class dataIo
{
public:

	bool readPointCloud(const std::string &fileName, pointCloudXYZ &pointCloud);	

protected:
	
private:
	
};
#endif
